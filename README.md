# Pecas, documentación

Aquí está la documentación de Pecas ([GitLab](https://gitlab.com/prolibreros/documentacion/pecas-legacy-docs)).

## Generación de la documentación

1. Tener [el repositorio de Pecas](https://gitlab.com/prolibreros/herramientas/pecas-legacy)
   en la misma ubicación de este repositorio.
2. Nombrar el repositorio de Pecas como `pecas`.
3. Tener [pecas instalado](https://programando.li/bros/).
4. Ingresar al directorio de este repositorio.
5. Ejecutar:

```
ruby src/create.rb
```

Esta generación también añade los `man` al directorio `docs` de  Pecas.

## Licencia

El contenido está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://gitlab.com/prolibreros/juridico/licencia-editorial-abierta-y-libre).
