# Pecas: herramientas editoriales (versión 0)

Pecas es un conjunto de _scripts_ que agilizan varios de los procesos del
quehacer editorial. Las herramientas están en [desarrollo
continuo](https://es.wikipedia.org/wiki/Liberaci%C3%B3n_continua) y siguien la
propuesta metodológica de la edición ramificada.

> ⚠️ Pecas v0 es _software_ [_legacy_](https://es.wikipedia.org/wiki/Sistema_heredado).

## Instalación

Para instalar solo basta con ejecutar:

```bash
curl -sSf https://gitlab.com/prolibreros/herramientas/pecas-legacy/-/raw/master/install.sh | $SHELL
```

Este comando clona a Pecas en `~/.local/opt/pecas` y en tu archivo de configuración
añade tres variables de entorno al `$PATH`, las cuales son necesarias
para la operación de Pecas.

Si quieres cambiar el directorio de instalación de Pecas lo puedes hacer
**si antes de la instalación** estableces la variable de entorno `$PECAS_HOME`
como se muestra en el siguiente ejemplo:

```bash
export PECAS_HOME=/home/usuario/otro/lugar
```

Para terminar, puedes verificar el estado de Pecas o sus [dependencias](https://es.wikipedia.org/wiki/Dependencias_de_software)
con [`pc-doctor`](html/pc-doctor.html).

## Utilización

Revisa el apartado de [herramientas](html/herramientas.html) para
conocer los diversos _scripts_ de Pecas.

## Solución de problemas

¿Estás teniendo dificultades con Pecas? Visita el apartado de [solución de problemas](html/problemas.html).

## Uso en Windows

¿Usas Windows y quieres usar Pecas? Visita [esta sección](html/problemas.html#como-uso-pecas-en-windows)
del apartado de solución de problemas.
